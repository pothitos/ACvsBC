# The Dilemma Between Arc and Bounds Consistency

 - This repository produces a
   [PDF](http://di.uoa.gr/~pothitos/papers/ACvsBC.pdf).
 - In this work, we are trying to track the cases when
   _bounds consistency_ (BC) is more efficient than _arc
   consistency_ (AC) in Constraint Programming.
 - The source code for the corresponding software is
   available in a separate
   [repository](https://github.com/pothitos/ACvsBC-Solver-Patches).

---

An [Open
Research](https://gist.github.com/pothitos/ec5f4f66ddd113aea6bac4094690d72e)
work
